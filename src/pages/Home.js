import Banner from '../components/Banner.js';
import {Image, Col} from 'react-bootstrap';
import {Fragment} from 'react';





export default function Home(){

	return(
		<Fragment>
			<Col fluid = "true" className="text-center  col-10 h-50 mx-auto mt-3">
				<Image className = "img-fluid logo col-6 mb-3" src ="https://i.ibb.co/z4gFYVj/knp-logo.png"/>
			</Col>
			<Banner/>
		</Fragment>


		)
}