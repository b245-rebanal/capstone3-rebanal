import {Table, Button} from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {useContext, Fragment} from 'react';
import {Navigate, Link,} from 'react-router-dom';




export default function ProductList ({allProductProp}){
const {  _id, name, description, price, isAvailable } = allProductProp;

const {user} = useContext(UserContext);





	return(
			
			user.isAdmin ?
			<Fragment>
			<Table striped bordered hover className="mb-0 text-center" >
		      <tbody>
		        <tr>
		          <td className = " col-3">{name}</td>
		          <td>{description}</td>
		          <td className = " col-1">PhP {price}</td>
		          {
		          	isAvailable?
		          <td className = " col-2">available</td>
		          :
		          <td className = " col-2">not available</td>
		      		}
		          <td className = " col-1">
		          	<Button as = {Link} to = {`/admin/updateProduct/${_id}`} className = "p-1 mb-1" variant="primary">Update</Button>
		          	{
		          		isAvailable?
		          		<Button  as = {Link} to = {`/admin/archiveProduct/${_id}`} className = "p-1" variant="danger" >Disable</Button>
		          		:
		          		<Button  as = {Link} to = {`/admin/archiveProduct/${_id}`} className = "p-1" variant="success" >Enable</Button>
		          	}
		          	
		          </td>
		          
		        </tr>
		        
		      </tbody>
		    </Table>
		    </Fragment>
		    :	
		    <Navigate to = "*"/>
		




		)
}