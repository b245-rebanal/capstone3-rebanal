
import {Form, Button, Container} from 'react-bootstrap';
import {Fragment, useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';




export default function AddProduct(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate()
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	

function addProduct(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/`,{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json',
			Authorization : `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			name:name,
			description:description,
			price:price
		})
	})
	.then(result => result.json())
	.then(data =>{
		console.log(data)

		if(data === false ){
			Swal.fire({
                    title: "Processing failed!",
                    icon: "error",
                    text: "Try again!"
                })
		}else{

		Swal.fire({
                    title: "Done!",
                    icon: 'success',
                    text: "New Product Added!"
                })
		navigate('/admin');
		}
	})
}

	return(
		<Fragment>
			<Container className="m-5 w-50 mx-auto">
				<h1>Add New Product!</h1>
				<Form  onSubmit = {event => addProduct(event)}>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Product Name</Form.Label>
				        <Form.Control 
					        type="text" 
					        placeholder="Product name" 
					        value ={name}
						    onChange ={event => setName(event.target.value)}
						     required
					        />

				      </Form.Group>
				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Description</Form.Label>
				        <Form.Control 
				        type="text" 
				        placeholder="Description"
				        value ={description}
					    onChange ={event => setDescription(event.target.value)}
					     required

				         />
				      </Form.Group>
				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Price</Form.Label>
				        <Form.Control
				        	type="text"
				        	placeholder="Price"
				        	value ={price}
						    onChange ={event => setPrice(event.target.value)}
						     required 
				        	/>
				      </Form.Group>

				      <Button variant="primary" type="submit">
				        Submit
				      </Button>
				    </Form>
			</Container>
		</Fragment>
		)
}