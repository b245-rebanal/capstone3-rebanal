import {Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useContext} from 'react';

import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom';


export default function ProductCard({productProp}){

	

	const { _id, name, description, price } = productProp;
		

	const [isDisabled] = useState(false);	



	const {user} = useContext(UserContext);

	
	return(
		<Row className = "mt-5">
			<Col className = "col-6 mx-auto text-center">
				<Card>
					<Card.Img variant="left" src="https://i.ibb.co/D1RVsy1/sample-img.jpg"/>
				    <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PhP {price}</Card.Text>

				        

				        {
				        	user ?
				        	<Button as = {Link} to = {`/product/${_id}`} disabled = {isDisabled}  variant="danger">see more details</Button>
				        	:
				        	<Button as = {Link} to ="/login"  variant="danger">Login</Button>
				        }


				        
				    </Card.Body>
				</Card>

			</Col>
		</Row>
	)
}