import React from "react";




export default function Footer(){

	const year = new Date().getFullYear();

  		return <footer fixed="bottom" className ="text-center h-50 mx-auto mt-3">
  		{`Copyright © Knead for Pastry ${year}`}
  		</footer>;
}
