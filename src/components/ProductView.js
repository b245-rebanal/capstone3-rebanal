

import {Fragment, useState, useEffect, useContext} from 'react';
import{Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useParams, useNavigate, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';




export default function ProductView (){
	const {user} = useContext(UserContext);
	const navigate = useNavigate()
;	
;	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState(0);
	const {productId} = useParams();

	  const incNum =()=>{
	    if(quantity<10)
	    {
	    setQuantity((quantity)+1);
	    }
	  };
	  const decNum = () => {
	     if(quantity>0)
	     {
	      setQuantity(quantity - 1);
	     }
	  }
	 const handleChange = (e)=>{
	   setQuantity(e.target.value);
	  }


	useEffect(()=>{

		fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data =>{
			
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
		})

	},[productId])

	const Checkout = (id) => {
		

		fetch(`${process.env.REACT_APP_KNEAD_FOR_PASTRY_URL}/user/order/${id}`,
		{
			method: 'POST',
			headers:{
				'Content-Type':'aplication/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`},
				body: JSON.stringify({

				quantity:quantity
			})
		})
		.then(result => result.json())
		.then(data => { 
			
			console.log(data);
			if(data===true){
				Swal.fire({
					title: 'Successfully added to cart!',
					icon: "success",
					text: "Please wait for the final schedule of the product"
				})
				
				navigate('/products');
			}else{
				Swal.fire({
					title: 'Transaction failed!',
					icon: "error",
					text: "Please try again"
				})
			}
	})
	}

	return(
		user?
		<Fragment>
		{
		user.isAdmin ?
		<Navigate to = "*"/>
		
        :
        <Fragment>
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                    	<Card.Img variant="left" src="https://i.ibb.co/D1RVsy1/sample-img.jpg"/>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>

                            <Container className="d-flex justify-content-center">
                            <Button variant="danger" className="ml-0" onClick = {decNum}>-</Button>
                            <Container className="col-2 p-0 mx-0">
                            <Form.Control className="text-center" type="text" value={quantity} onChange={handleChange} />
                            </Container>
                            <Button variant="danger" onClick = {incNum}>+</Button>
                            </Container>
                            <Button variant="danger" className="m-1" onClick = {()=> Checkout(productId)}>Check out</Button>
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
        </Fragment>
        }
		</Fragment>
		:
		<Navigate to = "/login"/>
		
		
)
}



